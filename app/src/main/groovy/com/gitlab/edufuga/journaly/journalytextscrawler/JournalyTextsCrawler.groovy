package com.gitlab.edufuga.journaly.journalytextscrawler

import groovy.transform.CompileStatic
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

@CompileStatic
class JournalyTextsCrawler {
    private final String baseURL = "https://journaly.com/post/"

    private final Path root

    JournalyTextsCrawler() {
        this.root = Paths.get(System.getProperty("user.home"))
                .resolve("Journaly")
                .resolve("Posts")
    }

    JournalyTextsCrawler(Path root) {
        this.root = root
    }

    void crawl(int from, int to) {
        assert to >= from

        for (int i in from..to) {
            String postURL = "${baseURL}$i"
            try {
                println "Crawling $postURL"
                Post post = crawlPost(postURL)

                Path destination = root
                        .resolve(post.getAuthor())
                        .resolve(post.getLanguage())
                        .resolve(post.getLanguageLevel())
                        .resolve("" + i)

                Files.createDirectories(destination)

                Path destinationPostFile = destination.resolve(post.getTitle() + ".txt")
                Files.deleteIfExists(destinationPostFile)
                Files.write(destinationPostFile, post.getText().getBytes(Charset.forName("UTF-8")))
            }
            catch (Exception ignored) {
                println "Post $postURL couldn't be crawled."
            }
        }
    }

    private Post crawlPost(String url) {
        Document doc = Jsoup.connect(url).get()
        String pageTitle = doc.title()
        def authorAndTitle = pageTitle.split("\\|")
        String author = authorAndTitle[0]
        String title = authorAndTitle[1]

        Elements langs = doc.select(".language")
        Element lang = langs.first()
        String language = lang.text()
        String languageLevel = "BEGINNER"
        for (Element l : langs.select("div")) {
            if (l.hasAttr("title")) {
                languageLevel = l.attr("title")
                break
            }
        }

        Element body = doc.select(".post-body").select("div").first()
        List<String> lines = new ArrayList<>()
        for (Element line : body.select("p")) {
            lines.add(line.text())
        }

        println "Number of lines: " + lines.size()
        String text = lines.join("\n")

        return new Post(author.trim(), language.trim(), languageLevel.trim(), title.trim(), text)
    }

    static void main(String[] args) {
        Path root = Files.createTempDirectory("Journaly")
        int from = 9
        int to = 29

        if (args.size() == 1) {
            root = Paths.get(args[0])
        }
        else if (args.size() == 2) {
            from = Integer.parseInt(args[0])
            to = Integer.parseInt(args[1])
        }
        else if (args.size() == 3) {
            root = Paths.get(args[0])
            from = Integer.parseInt(args[1])
            to = Integer.parseInt(args[2])
        }

        println "Crawling Journaly Posts into $root."
        new JournalyTextsCrawler(root).crawl(from, to)
    }
}
