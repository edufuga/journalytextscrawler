package com.gitlab.edufuga.journaly.journalytextscrawler

import groovy.transform.CompileStatic

@CompileStatic
class Post {
    final private String author
    final private String language
    final private String languageLevel
    final private String title
    final private String text

    Post(String author, String language, String languageLevel, String title, String text) {
        this.author = author
        this.language = language
        this.languageLevel = languageLevel
        this.title = title
        this.text = text
    }

    String getAuthor() {
        return author
    }

    String getLanguage() {
        return language
    }

    String getLanguageLevel() {
        return languageLevel
    }

    String getTitle() {
        return title
    }

    String getText() {
        return text
    }


    @Override
    String toString() {
        return "Post{" +
                "author='" + author + '\'' +
                ", language='" + language + '\'' +
                ", languageLevel='" + languageLevel + '\'' +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}