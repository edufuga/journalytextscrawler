# Journaly Crawler

This small project is a crawler for Journaly posts. It saves posts as
plain text files.

![Journaly Crawler](./Crawler.png)

## How to save all posts

To run the program, execute the following command:

    ./gradlew run

This saves all available posts between the post numbers 1 and 15.000.

## How to save a limited number of posts

To crawl the posts 20 to 30, run the following command:

    ./gradlew run --args="20 30"

## Resulting file structure

The resulting file structure looks like this:

![File structure](./Filestructure.png)

Every post is saved in a folder, including details about the author's
name, language and language level. Additionally, the post number is also
saved.
